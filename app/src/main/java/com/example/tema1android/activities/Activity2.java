package com.example.tema1android.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.tema1android.R;
import com.example.tema1android.fragments.F1A2;
import com.example.tema1android.fragments.F2A2;
import com.example.tema1android.fragments.F3A2;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, new F1A2(), "first");
        fragmentTransaction.commit();
    }
    public void addF2A2(View view) {

        Fragment f2a2 = new F2A2();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, f2a2);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void closeActivity(View view) {
        this.finish();
    }
    public void replace_F2A2_with_F3A2(View view) {
        Fragment f3a2 = new F3A2();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, f3a2);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public void go_to_FIA2(View view){
        super.onBackPressed();

        Toast toast = Toast.makeText(getApplicationContext(), "Back to F1A2", Toast.LENGTH_SHORT);
        toast.show();
    }
}