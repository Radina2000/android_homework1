package com.example.tema1android.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;

import com.example.tema1android.R;
import com.example.tema1android.fragments.F1A1;


public class Activity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();

        F1A1 f1a1 = new F1A1();
        fragmentTransaction.add(R.id.F1A1, f1a1);
        fragmentTransaction.commit();

        if(getIntent().getBooleanExtra("EXIT",false)){
            finish();
        }
    }
    public void closeActivity(View view) {
        closeActivity(view);
    }
    public void go_to_Activity2(View view)
    {
        Intent intent=new Intent(Activity1.this, Activity2.class);
        startActivity(intent);
        finish();
    }
}